# Adds CentOS 7 repo for Nvidias cuda
# FIXME: key validation?
class analysator::repos::cuda {
  package { 'cuda-repo-rhel7':
    ensure   => present,
    provider => rpm,
    source   => 'https://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-10.0.130-1.x86_64.rpm',
  }
}

