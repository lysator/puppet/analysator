class analysator::disable_autofs {
  # Disable autofs if installed
  exec { '/bin/systemctl disable autofs.service':
    onlyif => '/bin/systemctl is-enabled autofs',
  }
  exec { '/bin/systemctl stop autofs.service':
    onlyif => '/bin/systemctl is-active autofs',
  }
}
