class analysator::desktop {
  yum::group {
    [
      'X Window System',
      'Fonts',
      'GNOME Desktop',
      'Xfce',
      'MATE Desktop',
      'KDE Plasma Workspaces',
    ]:
      ensure  => present,
      timeout => 1200,
  }

  include analysator::packages::basic
  package {
    [
      'ghc-xmonad',
      'ghc-xmonad-contrib',
      'ghc-xmonad-contrib-devel',
      'ghc-xmonad-devel',
    ]:
      ensure => latest,
  }
}
