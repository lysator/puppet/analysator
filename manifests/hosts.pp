class analysator::hosts {
  require ::lysnetwork::hosts
  require ::lyslagring::hosts

  concat::fragment { '/etc/hosts/02-analysator':
    target => '/etc/hosts',
    source => 'puppet:///modules/analysator/hosts',
  }
}
