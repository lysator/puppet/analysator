class analysator::munge {
  package { 'munge':
    ensure => installed,
  }

  file { '/etc/munge/munge.key':
    ensure  => file,
    content => 'supersecretreallygoodprivatekey!',
    owner   => 'munge',
    group   => 'munge',
    mode    => '0400',
  }

  service { 'munge':
    ensure    => running,
    enable    => true,
    require   => [ Package['munge'], File['/etc/munge/munge.key'] ],
    subscribe => [ Package['munge'], File['/etc/munge/munge.key'] ],
  }
}
